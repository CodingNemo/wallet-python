from wallet import Amount, IsoCode, RateProvider, Wallet, Currency, Stock, StockType


class TestWallet:
    def test_should_return_0_for_empty_wallet(self):
        expected = Amount(euro(), 0)
        assert expected == Wallet().value(euro(), ConstantRate())

    def test_should_return_value_of_stock_for_wallet_with_one_currency(self):
        euro_stock = Stock.from_currency(euro(), 10)
        expected = Amount(euro(), 10)
        assert expected == Wallet().add(euro_stock).value(euro(), ConstantRate())

    def test_should_return_value_of_stock_for_wallet_with_two_currency(self):
        expected = Amount(euro(), 5 + 3 * 2)
        assert expected == sample_wallet().value(euro(), ConstantRate())


def sample_wallet():
    euro_stock = Stock.from_currency(euro(), 5)
    brent_stock = Stock(StockType.BRENT, 3)
    wallet = Wallet().add(euro_stock).add(brent_stock)
    return wallet


def euro():
    return Currency.from_iso_code(IsoCode.EUR)


class TestStockType:
    def test_euro(self):
        assert StockType.from_iso_code(IsoCode.EUR) == StockType.EURO

    def test_usd(self):
        assert StockType.from_iso_code(IsoCode.USD) == StockType.USD


class TestCurrency:
    def test_euro(self):
        assert Currency.from_iso_code(IsoCode.EUR).symbol == "€"
        assert Currency.from_iso_code(IsoCode.EUR).precision == 2

    def test_usd(self):
        assert Currency.from_iso_code(IsoCode.USD).symbol == "$"
        assert Currency.from_iso_code(IsoCode.USD).precision == 3


class ConstantRate(RateProvider):
    def rate(self, f: StockType, t: Currency):
        if f == StockType.EURO and t.iso_code == IsoCode.EUR:
            return 1
        return 2
