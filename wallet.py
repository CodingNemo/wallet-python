from enum import Enum
from typing import cast, List


class IsoCode(Enum):
    EUR = 'EUR'
    USD = 'USD'


class StockType(Enum):
    EURO = 'S:EUR'
    USD = 'S:USD'
    BRENT = 'Brent Petroleum'

    @staticmethod
    def from_iso_code(code: IsoCode) -> 'StockType':
        if code == IsoCode.EUR:
            return StockType.EURO
        if code == IsoCode.USD:
            return StockType.USD


class Currency:
    def __init__(self, code: IsoCode, symbol: str, precision) -> None:
        self.precision = precision
        self._iso_code = code
        self._symbol = symbol

    @staticmethod
    def from_iso_code(code: IsoCode) -> 'Currency':
        if code == IsoCode.EUR:
            return Currency(code, "€", 2)
        if code == IsoCode.USD:
            return Currency(code, "$", 3)

    def __str__(self) -> str:
        return self.symbol

    @property
    def symbol(self) -> str:
        return self._symbol

    @property
    def iso_code(self):
        return self._iso_code


class Amount:
    def __init__(self, currency: Currency, quantity: float) -> None:
        self._quantity = quantity
        self._currency = currency

    def __eq__(self, other: object) -> bool:
        other_amount = cast(Amount, other)
        return self.quantity == other_amount.quantity

    @property
    def quantity(self) -> float:
        return self._quantity

    @property
    def currency(self) -> Currency:
        return self._currency

    def __repr__(self) -> str:
        return "{} {}".format(self.quantity, self.currency)


class RateProvider:
    def rate(self, from_type: StockType, to: Currency) -> float:
        raise NotImplemented("Must be overwrite")


class Stock:
    def __init__(self, type: StockType, quantity: float) -> None:
        self._type = type
        self._quantity = quantity

    @staticmethod
    def from_currency(currency: Currency, quantity: float) -> 'Stock':
        stock_type = StockType.from_iso_code(currency.iso_code)
        return Stock(stock_type, quantity)

    @property
    def quantity(self) -> float:
        return self._quantity

    @property
    def type(self) -> StockType:
        return self._type


class Wallet:
    def __init__(self, stocks=None) -> None:
        if stocks is None:
            self._stocks = []
        else:
            self._stocks = stocks

    def value(self, currency: Currency, rate: RateProvider) -> Amount:
        total = sum([stock.quantity * rate.rate(stock.type, currency) for stock in self._stocks])
        return Amount(Currency.from_iso_code(IsoCode.EUR), total)

    def add(self, stock: Stock) -> "Wallet":
        return Wallet(self._stocks + [stock])
